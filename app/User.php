<?php

namespace App;

use App\Models\Link;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'referral_user_id', 'level', 'referral_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function link()
    {
        return $this->hasOne(Link::class, 'user_id');
    }

    public function referral()
    {
        return $this->hasMany(User::class, 'referral_user_id');
    }

    public function partner()
    {
        return $this->belongsTo(User::class, 'referral_user_id');
    }

    public function scopeCheckReferral($query)
    {
        return isset($query->whereHas('referral')->level);
    }

    public function scopeGetReferralLevel($query)
    {
        return $query->with('referral')->whereHas('referral')->level;
    }

    public function scopeGetReferralLink($query)
    {
        return $query->link->referral_link;
    }

    public function scopeFindReferralByLink($query, $referral_link)
    {
        return $query->with('link')->whereHas('link', function ($sub_query) use ($referral_link) {
           $sub_query->where('referral_link', $referral_link);
        });
    }
}

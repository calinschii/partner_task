<?php

namespace App\Listeners;

use App\Events\ReferralUpdateBalanceEvent;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdatePartnerBalanceListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(ReferralUpdateBalanceEvent $event)
    {
        if ($event->user->checkReferral()) {

            $percent = config('partner.percents.10');
            $amount = ($event->amount / $percent * 100);
            $referral = User::where('id', $event->user->referral->first()->id)->first();
            $this->updateBalance($referral->id, $amount);
            $this->updateBalance($event->user->id, ($event->amount - $amount));

            if ($event->user->referral->checkReferral()) {

                $percent = config('partner.percents.10');
                $amount_2 = (($event->amount - $amount) / $percent * 100);
                $referral = User::where('id', $event->user->referral->first()->referral->id)->first();
                $this->updateBalance($referral->id, $amount_2);
                $this->updateBalance($referral->id, ($event->amount - $amount - $amount_2));

            }
        } else {

            $this->updateBalance($event->user->id, $event->amount);
        }
    }

    public function updateBalance($user_id, $amount)
    {
        $user = User::find($user_id);
        $user_count = ($user->balance_count->count + $amount);
        return $user->balance_count->update(['count' => $user_count]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Events\ReferralUpdateBalanceEvent;
use App\Models\Link;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function updateCount(User $user, $amount)
    {
        /*Предположим что здесь уже есть функционал пополнения баланса*/

        /*
        $user = Авторизованный пользователь
        $amount = Сумма начисления
         */

        Event::fire(new ReferralUpdateBalanceEvent($user, $amount));
    }

    public function update(User $user, Request $request)
    {
        $user->update([
            'email' => $request->email,
            'name' => $request->name,
            'referral_code' => $request->referral_code
        ]);

        $referral_link = $this->generateLink($user);
        $this->saveReferralLink($referral_link, $user);
    }

    private function generateLink(User $user)
    {
        return env('APP_URL') . 'register/' . '?referral=' . Hash::make($user->referral_code);
    }

    private function saveReferralLink($link, User $user)
    {
        $new_link = Link::updateOrCreate(['user_id' => $user->id], [
            'referral_link' => $link
        ]);

        return $user->link()->save($new_link);
    }
}

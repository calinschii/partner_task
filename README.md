## Описание

По моему методу, обновление баланса рефералов происходит через событие.
```sh
ReferralUpdateBalanceEvent::class
```
и слушателя
```sh
UpdatePartnerBalanceListener::class
```
### Как это работает?

В конструктор события приходят 2 аргумента:
- Пользователь
- Кол-во средств для зачисления

Далее обработчик проверяет если у **пользователя** есть рефералы:

**Предположим, у пользователя имеется связная таблица с текущим балансом**
```sh
$user->balance_count
```
```sh
public function handle(ReferralUpdateBalanceEvent $event)
    {
        if ($event->user->checkReferral()) {

            $percent = config('partner.percents.10');
            $amount = ($event->amount / $percent * 100);
            $referral = User::where('id', $event->user->referral->first()->id)->first();
            $this->updateBalance($referral->id, $amount);
            $this->updateBalance($event->user->id, ($event->amount - $amount));

            if ($event->user->referral->checkReferral()) {

                $percent = config('partner.percents.10');
                $amount_2 = (($event->amount - $amount) / $percent * 100);
                $referral = User::where('id', $event->user->referral->first()->referral->id)->first();
                $this->updateBalance($referral->id, $amount_2);
                $this->updateBalance($referral->id, ($event->amount - $amount - $amount_2));

            }
        } else {

            $this->updateBalance($event->user->id, $event->amount);
        }
    }
```
### Функция обновления баланса
```sh
public function updateBalance($user_id, $amount)
    {
        $user = User::find($user_id);
        $user_count = ($user->balance_count->count + $amount);
        return $user->balance_count->update(['count' => $user_count]);
    }
```
## Функционал пользователя

**Сохранение реферального кода в личном кабинете**

```sh
$user->update([
            'email' => $request->email,
            'name' => $request->name,
            *'referral_code' => $request->referral_code
        ]);
```
**Генерация ссылки для регистрации**
```sh
$referral_link = $this->generateLink($user);
        $this->saveReferralLink($referral_link, $user);
```
```sh
private function generateLink(User $user)
    {
        return env('APP_URL') . 'register/' . '?referral=' . Hash::make($user->referral_code);
    }

    private function saveReferralLink($link, User $user)
    {
        $new_link = Link::updateOrCreate(['user_id' => $user->id], [
            'referral_link' => $link
        ]);

        return $user->link()->save($new_link);
    }
```
*Ссылка для регистрации имеет стандратный вид, но при этом вконце добавляем код реферала и хэшируем его*

**Регистрация ползователя**
```sh
protected function create(array $data, Request $request)
    {
        $partner_id = null;
        $level = null;

        if ($request->has('referral')) {

            $partner = User::findReferralByLink($request->fullUrl())->first();
            $partner_id = $partner->id;

            if ($partner->referall->first()) {

                $level = config('partner.referral.levels')[0];

            } elseif ($partner->referral->first()->referral) {

                $level = config('partner.referral.levels')[1];
            }
        }

        return User::create([
            ***
            'referral_user_id' => $partner_id,
            'level' => $level
        ]);
    }
```
*Проверяем уровень **level** реферала, получае и записываем ID реферала*
### Модель User
**Связь с ссылками для регистрации**
*Можем поучить созданную ссылку*
```sh
public function link()
    {
        return $this->hasOne(Link::class, 'user_id');
    }
```
**Связь с рефералами**
```sh
public function referral()
    {
        return $this->hasMany(User::class, 'referral_user_id');
    }
```
**Проверка на наличие реферала**
```sh
public function scopeCheckReferral($query)
    {
        return isset($query->whereHas('referral')->level);
    }
```
**Проверка уровня реферала**
```sh
public function scopeGetReferralLevel($query)
    {
        return $query->with('referral')->whereHas('referral')->level;
    }
```
**Получить ссылку реферала**
```sh
public function scopeGetReferralLink($query)
    {
        return $query->link->referral_link;
    }
```
**Поиск реферала по ссылке**
```sh
 public function scopeFindReferralByLink($query, $referral_link)
    {
        return $query->with('link')->whereHas('link', function ($sub_query) use ($referral_link) {
           $sub_query->where('referral_link', $referral_link);
        });
    }
```
## Миграции
#### для User
```sh
Schema::create('users', function (Blueprint $table) {
            ***
            $table->unsignedBigInteger('referral_user_id')->nullable()->index();
            $table->enum('level', config('partner.referral.levels'))->nullable();
            $table->string('referral_code')->nullable();
        });
```
**Level берём из файла конфигурации partner.php**
```sh
return [
    'referral' => [
        'levels' => [1, 2]
    ],
    'percents' => [
        '10' => 10,
        '5' => 5
    ]
];
```
#### для Link  
```sh
Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('referral_link');
            $table->timestamps();
        });
```
*При регистрации помещаем **user_id***



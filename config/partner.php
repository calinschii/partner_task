<?php
return [
    'referral' => [
        'levels' => [1, 2]
    ],
    'percents' => [
        '10' => 10,
        '5' => 5
    ]
];
